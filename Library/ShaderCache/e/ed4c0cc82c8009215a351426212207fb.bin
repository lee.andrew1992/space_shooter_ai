<Q                         _MAIN_LIGHT_SHADOWS    _MIXED_LIGHTING_SUBTRACTIVE    
   _NORMALMAP  2O  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(4) uniform UnityPerCamera {
#endif
	UNITY_UNIFORM vec4 _Time;
	UNITY_UNIFORM vec4 _SinTime;
	UNITY_UNIFORM vec4 _CosTime;
	UNITY_UNIFORM vec4 unity_DeltaTime;
	UNITY_UNIFORM vec4 _TimeParameters;
	UNITY_UNIFORM vec3 _WorldSpaceCameraPos;
	UNITY_UNIFORM vec4 _ProjectionParams;
	UNITY_UNIFORM vec4 _ScreenParams;
	UNITY_UNIFORM vec4 _ZBufferParams;
	UNITY_UNIFORM vec4 unity_OrthoParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM mediump vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(5) uniform UnityPerFrame {
#endif
	UNITY_UNIFORM mediump vec4 glstate_lightmodel_ambient;
	UNITY_UNIFORM mediump vec4 unity_AmbientSky;
	UNITY_UNIFORM mediump vec4 unity_AmbientEquator;
	UNITY_UNIFORM mediump vec4 unity_AmbientGround;
	UNITY_UNIFORM mediump vec4 unity_IndirectSpecColor;
	UNITY_UNIFORM vec4 unity_FogParams;
	UNITY_UNIFORM mediump vec4 unity_FogColor;
	UNITY_UNIFORM vec4 hlslcc_mtx4x4glstate_matrix_projection[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixV[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixInvV[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_MatrixVP[4];
	UNITY_UNIFORM vec4 unity_StereoScaleOffset;
	UNITY_UNIFORM int unity_StereoEyeIndex;
	UNITY_UNIFORM mediump vec4 unity_ShadowColor;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(2) uniform _Terrain {
#endif
	UNITY_UNIFORM mediump float _Metallic0;
	UNITY_UNIFORM mediump float _Metallic1;
	UNITY_UNIFORM mediump float _Metallic2;
	UNITY_UNIFORM mediump float _Metallic3;
	UNITY_UNIFORM mediump float _Smoothness0;
	UNITY_UNIFORM mediump float _Smoothness1;
	UNITY_UNIFORM mediump float _Smoothness2;
	UNITY_UNIFORM mediump float _Smoothness3;
	UNITY_UNIFORM vec4 _Control_ST;
	UNITY_UNIFORM mediump vec4 _Splat0_ST;
	UNITY_UNIFORM mediump vec4 _Splat1_ST;
	UNITY_UNIFORM mediump vec4 _Splat2_ST;
	UNITY_UNIFORM mediump vec4 _Splat3_ST;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(3) uniform _MainLightShadowBuffer {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres0;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres1;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres2;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres3;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSphereRadii;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset0;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset1;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset2;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset3;
	UNITY_UNIFORM mediump vec4 _MainLightShadowData;
	UNITY_UNIFORM vec4 _MainLightShadowmapSize;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out mediump vec4 vs_TEXCOORD3;
out mediump vec4 vs_TEXCOORD4;
out mediump vec4 vs_TEXCOORD5;
out mediump vec4 vs_TEXCOORD6;
out highp vec3 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD8;
vec4 u_xlat0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
vec3 u_xlat3;
vec3 u_xlat4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec2 u_xlat16;
mediump float u_xlat16_25;
void main()
{
    vs_TEXCOORD0.zw = in_TEXCOORD0.xy * unity_LightmapST.xy + unity_LightmapST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD1.xy = in_TEXCOORD0.xy * _Splat0_ST.xy + _Splat0_ST.zw;
    vs_TEXCOORD1.zw = in_TEXCOORD0.xy * _Splat1_ST.xy + _Splat1_ST.zw;
    vs_TEXCOORD2.xy = in_TEXCOORD0.xy * _Splat2_ST.xy + _Splat2_ST.zw;
    vs_TEXCOORD2.zw = in_TEXCOORD0.xy * _Splat3_ST.xy + _Splat3_ST.zw;
    u_xlat0.z = hlslcc_mtx4x4unity_ObjectToWorld[2].x;
    u_xlat0.x = hlslcc_mtx4x4unity_ObjectToWorld[0].x;
    u_xlat0.y = hlslcc_mtx4x4unity_ObjectToWorld[1].x;
    u_xlat16_1.x = dot(u_xlat0.xyz, in_NORMAL0.xyz);
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].y;
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].y;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].y;
    u_xlat16_1.y = dot(u_xlat2.xyz, in_NORMAL0.xyz);
    u_xlat3.z = hlslcc_mtx4x4unity_ObjectToWorld[2].z;
    u_xlat3.x = hlslcc_mtx4x4unity_ObjectToWorld[0].z;
    u_xlat3.y = hlslcc_mtx4x4unity_ObjectToWorld[1].z;
    u_xlat16_1.z = dot(u_xlat3.xyz, in_NORMAL0.xyz);
    u_xlat16_25 = dot(u_xlat16_1.xyz, u_xlat16_1.xyz);
    u_xlat16_25 = inversesqrt(u_xlat16_25);
    u_xlat16_1.xyz = vec3(u_xlat16_25) * u_xlat16_1.xyz;
    vs_TEXCOORD3.xyz = u_xlat16_1.xyz;
    u_xlat4.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat5.xyz = (-u_xlat4.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat16_25 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat16_25 = max(u_xlat16_25, 6.10351563e-05);
    u_xlat16_25 = inversesqrt(u_xlat16_25);
    u_xlat16_6.xyz = vec3(u_xlat16_25) * u_xlat5.xyz;
    vs_TEXCOORD3.w = u_xlat16_6.x;
    u_xlat16.xy = in_NORMAL0.yz * vec2(1.0, 0.0);
    u_xlat16.xy = in_NORMAL0.zx * vec2(0.0, 1.0) + (-u_xlat16.xy);
    u_xlat16_7.x = dot(u_xlat0.xy, u_xlat16.xy);
    u_xlat16_7.y = dot(u_xlat2.xy, u_xlat16.xy);
    u_xlat16_7.z = dot(u_xlat3.xy, u_xlat16.xy);
    u_xlat16_25 = dot(u_xlat16_7.xyz, u_xlat16_7.xyz);
    u_xlat16_25 = inversesqrt(u_xlat16_25);
    u_xlat16_7.xyz = vec3(u_xlat16_25) * u_xlat16_7.xyz;
    vs_TEXCOORD4.xyz = u_xlat16_7.xyz;
    vs_TEXCOORD4.w = u_xlat16_6.y;
    vs_TEXCOORD5.w = u_xlat16_6.z;
    u_xlat0.xyz = u_xlat16_1.zxy * u_xlat16_7.yzx;
    u_xlat0.xyz = u_xlat16_1.yzx * u_xlat16_7.zxy + (-u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xyz * unity_WorldTransformParams.www;
    vs_TEXCOORD5.xyz = u_xlat0.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD7.xyz = u_xlat4.xyz;
    u_xlat0 = u_xlat4.yyyy * hlslcc_mtx4x4_MainLightWorldToShadow[1];
    u_xlat0 = hlslcc_mtx4x4_MainLightWorldToShadow[0] * u_xlat4.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4_MainLightWorldToShadow[2] * u_xlat4.zzzz + u_xlat0;
    vs_TEXCOORD8 = u_xlat0 + hlslcc_mtx4x4_MainLightWorldToShadow[3];
    u_xlat0 = u_xlat4.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat4.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat4.zzzz + u_xlat0;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform _LightBuffer {
#endif
	UNITY_UNIFORM vec4 _MainLightPosition;
	UNITY_UNIFORM mediump vec4 _MainLightColor;
	UNITY_UNIFORM mediump vec4 _AdditionalLightsCount;
	UNITY_UNIFORM vec4 _AdditionalLightsPosition[16];
	UNITY_UNIFORM mediump vec4 _AdditionalLightsColor[16];
	UNITY_UNIFORM mediump vec4 _AdditionalLightsAttenuation[16];
	UNITY_UNIFORM mediump vec4 _AdditionalLightsSpotDir[16];
	UNITY_UNIFORM mediump vec4 _AdditionalLightsOcclusionProbes[16];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM mediump vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(2) uniform _Terrain {
#endif
	UNITY_UNIFORM mediump float _Metallic0;
	UNITY_UNIFORM mediump float _Metallic1;
	UNITY_UNIFORM mediump float _Metallic2;
	UNITY_UNIFORM mediump float _Metallic3;
	UNITY_UNIFORM mediump float _Smoothness0;
	UNITY_UNIFORM mediump float _Smoothness1;
	UNITY_UNIFORM mediump float _Smoothness2;
	UNITY_UNIFORM mediump float _Smoothness3;
	UNITY_UNIFORM vec4 _Control_ST;
	UNITY_UNIFORM mediump vec4 _Splat0_ST;
	UNITY_UNIFORM mediump vec4 _Splat1_ST;
	UNITY_UNIFORM mediump vec4 _Splat2_ST;
	UNITY_UNIFORM mediump vec4 _Splat3_ST;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(3) uniform _MainLightShadowBuffer {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres0;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres1;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres2;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSpheres3;
	UNITY_UNIFORM vec4 _CascadeShadowSplitSphereRadii;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset0;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset1;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset2;
	UNITY_UNIFORM mediump vec4 _MainLightShadowOffset3;
	UNITY_UNIFORM mediump vec4 _MainLightShadowData;
	UNITY_UNIFORM vec4 _MainLightShadowmapSize;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D _Control;
UNITY_LOCATION(2) uniform mediump sampler2D _Splat0;
UNITY_LOCATION(3) uniform mediump sampler2D _Splat1;
UNITY_LOCATION(4) uniform mediump sampler2D _Splat2;
UNITY_LOCATION(5) uniform mediump sampler2D _Splat3;
UNITY_LOCATION(6) uniform mediump sampler2D _Normal0;
UNITY_LOCATION(7) uniform mediump sampler2D _Normal1;
UNITY_LOCATION(8) uniform mediump sampler2D _Normal2;
UNITY_LOCATION(9) uniform mediump sampler2D _Normal3;
UNITY_LOCATION(10) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
uniform mediump sampler2D _MainLightShadowmapTexture;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD2;
in mediump vec4 vs_TEXCOORD3;
in mediump vec4 vs_TEXCOORD4;
in mediump vec4 vs_TEXCOORD5;
in highp vec4 vs_TEXCOORD8;
layout(location = 0) out mediump vec4 SV_TARGET0;
mediump vec4 u_xlat16_0;
mediump vec4 u_xlat16_1;
mediump vec4 u_xlat16_2;
mediump vec4 u_xlat16_3;
mediump float u_xlat16_4;
mediump vec4 u_xlat16_5;
mediump vec4 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_14;
mediump vec3 u_xlat16_15;
mediump float u_xlat16_25;
mediump float u_xlat16_30;
bool u_xlatb30;
mediump float u_xlat16_35;
mediump float u_xlat16_36;
void main()
{
    u_xlat16_0 = texture(_Splat2, vs_TEXCOORD2.xy);
    u_xlat16_1 = texture(_Splat0, vs_TEXCOORD1.xy);
    u_xlat16_2 = texture(_Splat1, vs_TEXCOORD1.zw);
    u_xlat16_3 = texture(_Control, vs_TEXCOORD0.xy);
    u_xlat16_4 = dot(u_xlat16_3, vec4(1.0, 1.0, 1.0, 1.0));
    u_xlat16_5.x = u_xlat16_4 + 6.10351563e-05;
    u_xlat16_3 = u_xlat16_3 / u_xlat16_5.xxxx;
    u_xlat16_5.x = u_xlat16_3.y;
    u_xlat16_6 = u_xlat16_3.yzwx * vec4(_Smoothness1, _Smoothness2, _Smoothness3, _Smoothness0);
    u_xlat16_5.w = u_xlat16_6.x;
    u_xlat16_2 = u_xlat16_2 * u_xlat16_5.xxxw;
    u_xlat16_6.x = u_xlat16_3.x;
    u_xlat16_1 = u_xlat16_1 * u_xlat16_6.xxxw + u_xlat16_2;
    u_xlat16_6.w = u_xlat16_3.z;
    u_xlat16_0 = u_xlat16_0 * u_xlat16_6.wwwy + u_xlat16_1;
    u_xlat16_1 = texture(_Splat3, vs_TEXCOORD2.zw);
    u_xlat16_6.w = u_xlat16_3.w;
    u_xlat16_0 = u_xlat16_1 * u_xlat16_6.wwwz + u_xlat16_0;
    u_xlat16_5.x = (-u_xlat16_0.w) + 1.0;
    u_xlat16_15.x = (-u_xlat16_5.x) * 0.699999988 + 1.70000005;
    u_xlat16_15.x = u_xlat16_15.x * u_xlat16_5.x;
    u_xlat16_5.x = u_xlat16_5.x * u_xlat16_5.x;
    u_xlat16_15.x = u_xlat16_15.x * 6.0;
    u_xlat16_1.xyz = texture(_Normal0, vs_TEXCOORD1.xy).xyz;
    u_xlat16_2.xyz = texture(_Normal1, vs_TEXCOORD1.zw).xyz;
    u_xlat16_2.xyz = u_xlat16_3.yyy * u_xlat16_2.xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz * u_xlat16_6.xxx + u_xlat16_2.xyz;
    u_xlat16_2.xyz = texture(_Normal2, vs_TEXCOORD2.xy).xyz;
    u_xlat16_1.xyz = u_xlat16_2.xyz * u_xlat16_3.zzz + u_xlat16_1.xyz;
    u_xlat16_2.xyz = texture(_Normal3, vs_TEXCOORD2.zw).xyz;
    u_xlat16_1.xyz = u_xlat16_2.xyz * u_xlat16_3.www + u_xlat16_1.xyz;
    u_xlat16_25 = dot(u_xlat16_3, vec4(_Metallic0, _Metallic1, _Metallic2, _Metallic3));
    u_xlat16_6.xyz = u_xlat16_1.xyz * vec3(2.0, 2.0, 2.0) + vec3(-1.0, -1.0, -1.0);
    u_xlat16_7.xyz = u_xlat16_6.yyy * vs_TEXCOORD5.xyz;
    u_xlat16_6.xyw = u_xlat16_6.xxx * vs_TEXCOORD4.xyz + u_xlat16_7.xyz;
    u_xlat16_1.xyz = u_xlat16_6.zzz * vs_TEXCOORD3.xyz + u_xlat16_6.xyw;
    u_xlat16_35 = dot(u_xlat16_1.xyz, u_xlat16_1.xyz);
    u_xlat16_35 = inversesqrt(u_xlat16_35);
    u_xlat16_2.xyz = u_xlat16_1.xyz * vec3(u_xlat16_35);
    u_xlat16_6.x = vs_TEXCOORD3.w;
    u_xlat16_6.y = vs_TEXCOORD4.w;
    u_xlat16_6.z = vs_TEXCOORD5.w;
    u_xlat16_35 = dot((-u_xlat16_6.xyz), u_xlat16_2.xyz);
    u_xlat16_35 = u_xlat16_35 + u_xlat16_35;
    u_xlat16_7.xyz = u_xlat16_2.xyz * (-vec3(u_xlat16_35)) + (-u_xlat16_6.xyz);
    u_xlat16_3 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, u_xlat16_15.x);
    u_xlat16_15.x = u_xlat16_3.w + -1.0;
    u_xlat16_15.x = unity_SpecCube0_HDR.w * u_xlat16_15.x + 1.0;
    u_xlat16_15.x = max(u_xlat16_15.x, 0.0);
    u_xlat16_15.x = log2(u_xlat16_15.x);
    u_xlat16_15.x = u_xlat16_15.x * unity_SpecCube0_HDR.y;
    u_xlat16_15.x = exp2(u_xlat16_15.x);
    u_xlat16_15.x = u_xlat16_15.x * unity_SpecCube0_HDR.x;
    u_xlat16_7.xyz = u_xlat16_3.xyz * u_xlat16_15.xxx;
    u_xlat16_15.x = u_xlat16_5.x * u_xlat16_5.x + 1.0;
    u_xlat16_15.x = float(1.0) / u_xlat16_15.x;
    u_xlat16_14.xyz = u_xlat16_7.xyz * u_xlat16_15.xxx;
    u_xlat16_15.x = (-u_xlat16_25) * 0.959999979 + 0.959999979;
    u_xlat16_35 = (-u_xlat16_15.x) + 1.0;
    u_xlat16_7.xyz = u_xlat16_0.xyz * u_xlat16_15.xxx;
    u_xlat16_15.x = u_xlat16_0.w + u_xlat16_35;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_15.x = min(max(u_xlat16_15.x, 0.0), 1.0);
#else
    u_xlat16_15.x = clamp(u_xlat16_15.x, 0.0, 1.0);
#endif
    u_xlat16_8.xyz = u_xlat16_0.xyz + vec3(-0.0399999991, -0.0399999991, -0.0399999991);
    u_xlat16_8.xyz = vec3(u_xlat16_25) * u_xlat16_8.xyz + vec3(0.0399999991, 0.0399999991, 0.0399999991);
    u_xlat16_15.xyz = u_xlat16_15.xxx + (-u_xlat16_8.xyz);
    u_xlat16_36 = dot(u_xlat16_2.xyz, u_xlat16_6.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_36 = min(max(u_xlat16_36, 0.0), 1.0);
#else
    u_xlat16_36 = clamp(u_xlat16_36, 0.0, 1.0);
#endif
    u_xlat16_6.xyz = u_xlat16_6.xyz + _MainLightPosition.xyz;
    u_xlat16_36 = (-u_xlat16_36) + 1.0;
    u_xlat16_36 = u_xlat16_36 * u_xlat16_36;
    u_xlat16_36 = u_xlat16_36 * u_xlat16_36;
    u_xlat16_15.xyz = vec3(u_xlat16_36) * u_xlat16_15.xyz + u_xlat16_8.xyz;
    u_xlat16_0.xyz = u_xlat16_14.xyz * u_xlat16_15.xyz;
    u_xlat16_15.x = u_xlat16_1.y * u_xlat16_1.y;
    u_xlat16_15.x = u_xlat16_1.x * u_xlat16_1.x + (-u_xlat16_15.x);
    u_xlat16_3 = u_xlat16_1.yzzx * u_xlat16_1.xyzz;
    u_xlat16_9.x = dot(unity_SHBr, u_xlat16_3);
    u_xlat16_9.y = dot(unity_SHBg, u_xlat16_3);
    u_xlat16_9.z = dot(unity_SHBb, u_xlat16_3);
    u_xlat16_15.xyz = unity_SHC.xyz * u_xlat16_15.xxx + u_xlat16_9.xyz;
    u_xlat16_1.w = 1.0;
    u_xlat16_9.x = dot(unity_SHAr, u_xlat16_1);
    u_xlat16_9.y = dot(unity_SHAg, u_xlat16_1);
    u_xlat16_9.z = dot(unity_SHAb, u_xlat16_1);
    u_xlat16_15.xyz = u_xlat16_15.xyz + u_xlat16_9.xyz;
    u_xlat16_15.xyz = max(u_xlat16_15.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_2.w = 1.0;
    u_xlat16_9.x = dot(unity_SHAr, u_xlat16_2);
    u_xlat16_9.y = dot(unity_SHAg, u_xlat16_2);
    u_xlat16_9.z = dot(unity_SHAb, u_xlat16_2);
    u_xlat16_15.xyz = u_xlat16_15.xyz + u_xlat16_9.xyz;
    u_xlat16_15.xyz = max(u_xlat16_15.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_0.xyz = u_xlat16_15.xyz * u_xlat16_7.xyz + u_xlat16_0.xyz;
    u_xlat16_15.x = dot(u_xlat16_6.xyz, u_xlat16_6.xyz);
    u_xlat16_15.x = max(u_xlat16_15.x, 6.10351563e-05);
    u_xlat16_15.x = inversesqrt(u_xlat16_15.x);
    u_xlat16_15.xyz = u_xlat16_15.xxx * u_xlat16_6.xyz;
    u_xlat16_6.x = dot(_MainLightPosition.xyz, u_xlat16_15.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_6.x = min(max(u_xlat16_6.x, 0.0), 1.0);
#else
    u_xlat16_6.x = clamp(u_xlat16_6.x, 0.0, 1.0);
#endif
    u_xlat16_15.x = dot(u_xlat16_2.xyz, u_xlat16_15.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_15.x = min(max(u_xlat16_15.x, 0.0), 1.0);
#else
    u_xlat16_15.x = clamp(u_xlat16_15.x, 0.0, 1.0);
#endif
    u_xlat16_25 = dot(u_xlat16_2.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_25 = min(max(u_xlat16_25, 0.0), 1.0);
#else
    u_xlat16_25 = clamp(u_xlat16_25, 0.0, 1.0);
#endif
    u_xlat16_15.x = u_xlat16_15.x * u_xlat16_15.x;
    u_xlat16_35 = u_xlat16_6.x * u_xlat16_6.x;
    u_xlat16_30 = max(u_xlat16_35, 0.100000001);
    u_xlat16_14.x = u_xlat16_5.x * u_xlat16_5.x + -1.0;
    u_xlat16_14.x = u_xlat16_15.x * u_xlat16_14.x + 1.00001001;
    u_xlat16_15.x = u_xlat16_14.x * u_xlat16_14.x;
    u_xlat16_30 = u_xlat16_30 * u_xlat16_15.x;
    u_xlat16_14.x = u_xlat16_5.x * 4.0 + 2.0;
    u_xlat16_5.x = u_xlat16_5.x * u_xlat16_5.x;
    u_xlat16_30 = u_xlat16_30 * u_xlat16_14.x;
    u_xlat16_30 = u_xlat16_5.x / u_xlat16_30;
    u_xlat16_5.x = u_xlat16_30 + -6.10351563e-05;
    u_xlat16_5.x = max(u_xlat16_5.x, 0.0);
    u_xlat16_5.x = min(u_xlat16_5.x, 100.0);
    u_xlat16_5.xyw = u_xlat16_5.xxx * u_xlat16_8.xyz + u_xlat16_7.xyz;
    vec3 txVec0 = vec3(vs_TEXCOORD8.xy,vs_TEXCOORD8.z);
    u_xlat16_30 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    u_xlat16_6.x = (-_MainLightShadowData.x) + 1.0;
    u_xlat16_6.x = u_xlat16_30 * _MainLightShadowData.x + u_xlat16_6.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb30 = !!(vs_TEXCOORD8.z>=1.0);
#else
    u_xlatb30 = vs_TEXCOORD8.z>=1.0;
#endif
    u_xlat16_6.x = (u_xlatb30) ? 1.0 : u_xlat16_6.x;
    u_xlat16_6.x = u_xlat16_6.x * unity_LightData.z;
    u_xlat16_25 = u_xlat16_25 * u_xlat16_6.x;
    u_xlat16_6.xyz = vec3(u_xlat16_25) * _MainLightColor.xyz;
    u_xlat16_5.xyz = u_xlat16_5.xyw * u_xlat16_6.xyz + u_xlat16_0.xyz;
    SV_TARGET0.xyz = vec3(u_xlat16_4) * u_xlat16_5.xyz;
    SV_TARGET0.w = 1.0;
    return;
}

#endif
                               _LightBuffer0        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                    0      _AdditionalLightsColor                   0     _AdditionalLightsAttenuation                 0     _AdditionalLightsSpotDir                 0      _AdditionalLightsOcclusionProbes                 0         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          _Terrainp      
   _Metallic0                        
   _Metallic1                       
   _Metallic2                       
   _Metallic3                          _Smoothness0                        _Smoothness1                        _Smoothness2                        _Smoothness3                        _Control_ST                       
   _Splat0_ST                    0   
   _Splat1_ST                    @   
   _Splat2_ST                    P   
   _Splat3_ST                    `          _MainLightShadowBuffer  �        _CascadeShadowSplitSpheres0                   @     _CascadeShadowSplitSpheres1                   P     _CascadeShadowSplitSpheres2                   `     _CascadeShadowSplitSpheres3                   p     _CascadeShadowSplitSphereRadii                    �     _MainLightShadowOffset0                   �     _MainLightShadowOffset1                   �     _MainLightShadowOffset2                   �     _MainLightShadowOffset3                   �     _MainLightShadowData                  �     _MainLightShadowmapSize                   �     _MainLightWorldToShadow                            UnityPerCamera  �   
      _Time                            _SinTime                        _CosTime                         unity_DeltaTime                   0      _TimeParameters                   @      _WorldSpaceCameraPos                  P      _ProjectionParams                     `      _ScreenParams                     p      _ZBufferParams                    �      unity_OrthoParams                     �          UnityPerFrame   �        glstate_lightmodel_ambient                           unity_AmbientSky                        unity_AmbientEquator                         unity_AmbientGround                   0      unity_IndirectSpecColor                   @      unity_FogParams                   P      unity_FogColor                    `      unity_StereoScaleOffset                   p     unity_StereoEyeIndex                 �     unity_ShadowColor                     �     glstate_matrix_projection                    p      unity_MatrixV                    �      unity_MatrixInvV                 �      unity_MatrixVP                   0            unity_SpecCube0                   _Control                _Splat0                 _Splat1                 _Splat2                 _Splat3                 _Normal0                _Normal1                _Normal2                _Normal3    	   	         _MainLightShadowmapTexture      
   
         _LightBuffer              UnityPerDraw             _Terrain             _MainLightShadowBuffer               UnityPerCamera               UnityPerFrame             