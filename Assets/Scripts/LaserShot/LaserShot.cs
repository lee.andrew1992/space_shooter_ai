﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class LaserShot : MonoBehaviour
{
    bool alive = false;
    float shotSpeed;
    float shotDuration;
    float shotCooldown;
    float currDuration = 0;
    float damage;
    int sourceLayer;
    public GameObject beam;
    public GameObject trail;
    public GameObject hitSpark;
    public float seekingRotateSpeed;
    public float seekingAngle;

    GameObject currentTarget = null;

    // Update is called once per frame
    void Update()
    {
        if (alive)
        {
            currDuration += Time.deltaTime;
            if (currDuration > shotDuration)
            {
                Destroy(this.gameObject);
            }
            else
            {
                transform.position += transform.forward * (shotSpeed * Time.deltaTime);
                if (currentTarget != null && UtilityFunctions.IsInFront(this.transform.gameObject, currentTarget, seekingAngle))
                {
                    var targetRotation = Quaternion.LookRotation(currentTarget.transform.position - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * seekingRotateSpeed);
                }
            }
        }
    }

    public void Instantiate(float shotSpeed, float shotDuration, float shotCooldown, float damage, int sourceLayer, Material material, GameObject target = null)
    {
        this.alive = true;
        this.shotSpeed = shotSpeed;
        this.shotDuration = shotDuration;
        this.shotCooldown = shotCooldown;
        this.damage = damage;
        this.sourceLayer = sourceLayer;
        beam.GetComponent<Renderer>().material = material;
        trail.GetComponent<TrailRenderer>().material = material;
        currentTarget = target;
    }

    void OnTriggerEnter(Collider other)
    {
        EntityController entityController;

        if (other.gameObject.layer != sourceLayer && UtilityFunctions.FindScriptInParent<EntityController>(other.gameObject, other.gameObject.layer, out entityController))
        {
            entityController.TakeDamage(damage);
            Debug.Log(other.gameObject.name);
            Quaternion hitSparkRotation = Quaternion.LookRotation(-transform.forward);
            Instantiate(hitSpark, transform.position, hitSparkRotation, entityController.transform);

            Destroy(this.gameObject);
        }
    }
}
