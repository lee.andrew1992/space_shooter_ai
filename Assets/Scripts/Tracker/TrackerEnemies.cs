﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerEnemies : MonoBehaviour
{
    private List<GameObject> empireShips = new List<GameObject>();
    private List<GameObject> rebelShips = new List<GameObject>();

    private const int EMPIRE = (int)MissionManager.Layers.Empire;
    private const int REBEL = (int)MissionManager.Layers.Rebellion;

    public GameObject teamEmpire;
    public GameObject teamRebels;

    public void AddShips(GameObject ship)
    {
        if (ship.layer == EMPIRE)
        {
            empireShips.Add(ship);
            ship.transform.parent = teamEmpire.transform;
        }

        else if (ship.layer == REBEL)
        {
            rebelShips.Add(ship);
            ship.transform.parent = teamRebels.transform;
        }

    }

    public List<GameObject> GetEmpireShips()
    {
        return empireShips;
    }

    public List<GameObject> GetRebelShips()
    {
        return rebelShips;
    }

    public void HandleDeath(GameObject entity)
    {
        if (entity.layer == EMPIRE)
        {
            empireShips.Remove(entity);
        }

        else if (entity.layer == REBEL)
        {
            rebelShips.Remove(entity);
        }
    }
}
