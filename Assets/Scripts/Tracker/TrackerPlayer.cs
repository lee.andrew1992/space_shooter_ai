﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackerPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject playerShip;

    public void SetPlayerShip(GameObject ship)
    {
        playerShip = ship;
    }

    public GameObject GetPlayerShip()
    {
        return playerShip;
    }

    public Camera GetPlayerCamera()
    {
        return playerShip.transform.GetChild(0).GetComponent<Camera>();
    }
}
