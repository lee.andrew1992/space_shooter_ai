﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PlanetEntry : PlanetTransition
{
    public Vector3 planetEntryPoint;
    public Vector3 planetEntryLookAt;

    public GameObject planet;
    public Camera mainCam;
    private PostProcessVolume postProcessing;

    public float planetDetectionRange;
    
    int layerMask = 1 << (int)MissionManager.Layers.Planets;
    float prevDistance;

    // Start is called before the first frame update
    void Start()
    {
        postProcessing = mainCam.GetComponent<PostProcessVolume>();
        prevDistance = planetDetectionRange;
        //ResetBurnEffectValues();
    }

    // Update is called once per frame
    void Update()
    {
        if (MissionManager.Instance.currLevel == MissionManager.CurrLevel.Space)
        {
            if (planet != null)
            {
                RaycastHit hit;
                Vector3 heading = planet.transform.position - this.transform.position;

                if (Physics.Raycast(this.transform.position, heading, out hit, planetDetectionRange, layerMask))
                {
                    float playerDistance = Vector3.Distance(hit.transform.position, this.transform.position) - (hit.transform.localScale.z / 2);

                    if (UtilityFunctions.IsInFront(this.gameObject, planet, 45) && playerDistance < prevDistance)
                    {
                        if (playerDistance < planetEnterRange)
                        {
                            EnterPlanet();
                        }

                        else if (playerDistance < planetEnterEffectRange)
                        {
                            IncreaseEffect(burnRingEffect, burnRingIntensity, burnRingSpeed);
                            IncreaseEffect(burnEffect, burnEffectIntensity, burnEffectSpeed);
                        }
                    }
                    else
                    {
                        DecreaseEffect(burnRingEffect);
                        DecreaseEffect(burnEffect);
                    }
                    prevDistance = playerDistance;
                }
            }
        }
    }

    void EnterPlanet()
    {
        MissionManager.Instance.miscTracker.Planet.GetComponent<PlanetLogic>().EnterLevel();

        MissionManager.Instance.miscTracker.Planet.SetActive(false);
        this.transform.position = planetEntryPoint;
        this.transform.LookAt(planetEntryLookAt);

        HideAndTurnOffAllAi();

        prevDistance = planetDetectionRange;

        StartCoroutine(DecreaseEffect(burnEffect));
        StartCoroutine(DecreaseEffect(burnRingEffect));
    }

    void HideAndTurnOffAllAi()
    {
        foreach (GameObject unit in MissionManager.Instance.enemiesTracker.GetEmpireShips())
        {
            unit.SetActive(false);
        }

        foreach (GameObject unit in MissionManager.Instance.enemiesTracker.GetRebelShips())
        {
            unit.SetActive(false);
        }
    }
}
