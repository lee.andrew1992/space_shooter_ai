﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlanetExit : PlanetTransition
{
    float previousHeight;

    // Start is called before the first frame update
    void Start()
    {
        previousHeight = planetExitEffectRange;
        ResetBurnEffectValues();
    }

    // Update is called once per frame
    void Update()
    {
        if (MissionManager.Instance.currLevel == MissionManager.CurrLevel.Planet)
        {
            float currentHeight = this.transform.position.y;

            if (currentHeight > previousHeight)
            {
                if (currentHeight > planetExitRange)
                {
                    ExitPlanet();
                }
                else if (currentHeight > planetExitEffectRange)
                {
                    IncreaseEffect(burnEffect, burnEffectIntensity, burnEffectSpeed);
                    IncreaseEffect(burnRingEffect, burnRingIntensity, burnRingSpeed);
                }
            }
            previousHeight = currentHeight;
        }
    }

    void ExitPlanet()
    {
        MissionManager.Instance.spaceLogic.EnterLevel();
        GameObject planet = MissionManager.Instance.miscTracker.Planet;
        this.transform.position = new Vector3 (
            600, 
            planet.transform.GetChild(0).localScale.y / 2 + planetEnterEffectRange, 
            400);
        planet.SetActive(true);

        ShowAndTurnOnAllAi();

        StartCoroutine(DecreaseEffect(burnEffect));
        StartCoroutine(DecreaseEffect(burnRingEffect));

        previousHeight = planetExitEffectRange;
    }

    void ShowAndTurnOnAllAi()
    {
        foreach (GameObject unit in MissionManager.Instance.enemiesTracker.GetEmpireShips())
        {
            unit.SetActive(true);
            if (unit.GetComponent<EntityControllerAI>().shipType == EntityControllerAI.AIType.normal)
                unit.GetComponent<AIMoveController>().ResetAi();
        }

        foreach (GameObject unit in MissionManager.Instance.enemiesTracker.GetRebelShips())
        {
            unit.SetActive(true);
            if (unit.GetComponent<EntityControllerAI>().shipType == EntityControllerAI.AIType.normal)
                unit.GetComponent<AIMoveController>().ResetAi();
        }
    }
}
