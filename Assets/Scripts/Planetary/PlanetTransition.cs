﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetTransition : MonoBehaviour
{
    [Header("Burn Ring Max Values")]
    public float burnRingIntensity;
    public float burnRingSpeed;

    [Header("Burn Max Values")]
    public float burnEffectIntensity;
    public float burnEffectSpeed;

    public Material burnRingEffect;
    public Material burnEffect;

    public float planetEnterRange;
    public float planetEnterEffectRange;

    public float planetExitRange;
    public float planetExitEffectRange;

    protected const string INTENSITY = "_Intensity";
    protected const string SPEED = "_Speed";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected IEnumerator DecreaseEffect(Material effect)
    {
        float currentIntensity = effect.GetFloat(INTENSITY);
        float currentSpeed = effect.GetFloat(SPEED);

        while (currentIntensity > 0)
        {
            currentIntensity = Mathf.Max(currentIntensity - Time.deltaTime / 20, 0);

            effect.SetFloat(INTENSITY, currentIntensity);
            effect.SetFloat(SPEED, currentSpeed);
            yield return null;
        }
        ResetBurnEffectValues();
    }

    protected void ResetBurnEffectValues()
    {
        burnRingEffect.SetFloat(INTENSITY, 0);
        burnRingEffect.SetFloat(SPEED, 0);

        burnEffect.SetFloat(INTENSITY, 0);
        burnEffect.SetFloat(SPEED, 0);
    }

    protected void IncreaseEffect(Material effect, float maxIntensity, float maxSpeed)
    {
        float currentIntensity = effect.GetFloat(INTENSITY);
        float currentSpeed = effect.GetFloat(SPEED);

        currentIntensity = Mathf.Min(currentIntensity + Time.deltaTime / 20, maxIntensity);
        currentSpeed = Mathf.Min(currentSpeed + Time.deltaTime / 20, maxSpeed);

        effect.SetFloat(INTENSITY, currentIntensity);
        effect.SetFloat(SPEED, currentSpeed);
    }
}
