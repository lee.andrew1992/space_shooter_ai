﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceLogic : LevelLogic
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public override void EnterLevel()
    {
        MissionManager.Instance.currLevel = MissionManager.CurrLevel.Space;
        base.EnterLevel();
    }
}
