﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KillParticleEffect : MonoBehaviour
{
    public ParticleSystem ps;
    private float durationMax;
    private float durationCurrent;

    // Start is called before the first frame update
    void Start()
    {
        durationMax = ps.main.duration;
    }

    // Update is called once per frame
    void Update()
    {
        durationCurrent += Time.deltaTime;

        if (durationCurrent > durationMax)
        {
            GameObject.Destroy(this);
        }
    }
}
