﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

[RequireComponent(typeof(EntityController))]
public class ControlsSetTarget : MonoBehaviour
{
    EntityController entityController;
    float currentSpeed = 0;
    private GameObject myTarget;
    private GameObject lastTarget;
    public GameObject targetArea;
    public List<GameObject> pursuers;

    public float pursuedFor;
    
    void Start()
    {
        myTarget = null;
        lastTarget = null;
        entityController = this.GetComponent<EntityController>();
        pursuedFor = 0;

        switch (entityController.currentTeam)
        {
            case MissionManager.Layers.Empire:
                targetArea.layer = (int)MissionManager.Layers.EmpireTargetArea;
                break;

            case MissionManager.Layers.Rebellion:
                targetArea.layer = (int)MissionManager.Layers.RebellionTargetArea;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    private AIMoveController GetAIController(GameObject gameObject)
    {
        if (gameObject.GetComponent<AIMoveController>() != null)
        {
            return gameObject.GetComponent<AIMoveController>();
        }

        if (gameObject.transform.parent.gameObject.GetComponent<AIMoveController>() != null)
        {
            return gameObject.transform.parent.GetComponent<AIMoveController>();
        }

        return null;
    }
}
