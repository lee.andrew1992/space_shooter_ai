﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

[RequireComponent(typeof(EntityController))]
public class ControlsMovement : MonoBehaviour
{
    EntityController entityController;
    Rigidbody rigidBody;

    float rjsHorizontal;
    float rjsVertical;
    float ljsHorizontal;
    float ljsVertical;

    float currentSpeed = 0;

    public Joystick leftJoystick;
    public Joystick rightJoystick;
    public Joystick weaponJoystick;

    public float controlDeadzone;

    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody>();
        entityController = this.GetComponent<EntityController>();
    }

    // Update is called once per frame
    void Update()
    {
        ApplyDeadzone(leftJoystick, true);

        if (rightJoystick.Horizontal != 0 || rightJoystick.Vertical != 0)
        {
            ApplyDeadzone(rightJoystick, false);
            weaponJoystick.gameObject.SetActive(false);
        }

        if (weaponJoystick.Horizontal != 0 || weaponJoystick.Vertical != 0)
        {
            ApplyDeadzone(weaponJoystick, false);
            rightJoystick.gameObject.SetActive(false);
        }

        if (rightJoystick.Horizontal == 0 && rightJoystick.Vertical == 0 && weaponJoystick.Horizontal == 0 && weaponJoystick.Vertical == 0)
        {
            rjsHorizontal = 0;
            rjsVertical = 0;

            weaponJoystick.gameObject.SetActive(true);
            rightJoystick.gameObject.SetActive(true);
        }

        float rotationY = -rjsHorizontal * Time.deltaTime * entityController.rotateSpeed;
        float rotationX = rjsVertical * Time.deltaTime * entityController.rotateSpeed;
        float rotationZ = -ljsHorizontal * Time.deltaTime * entityController.tiltSpeed;

        float speedInput = ljsVertical * Time.deltaTime * entityController.moveSpeedMultiplier;

        if (speedInput > 0)
        {
            currentSpeed = Mathf.Min(currentSpeed + speedInput, entityController.moveSpeedMax);
        }
        else if (speedInput < 0)
        {
            currentSpeed = Mathf.Max(currentSpeed + speedInput, -entityController.moveSpeedMax / 2);
        }
        else
        {
            currentSpeed = Mathf.Lerp(currentSpeed, 0, Time.deltaTime);
        }

        this.transform.RotateAround(this.transform.position, transform.forward, rotationZ);
        this.transform.RotateAround(this.transform.position, -transform.right, rotationX);
        this.transform.RotateAround(this.transform.position, -transform.up, rotationY);

        rigidBody.velocity = this.transform.forward * (this.entityController.moveSpeedMultiplier * currentSpeed * Time.deltaTime);

        // This is for a diff. move style
        //this.transform.position += (this.transform.forward * currentSpeed * Time.deltaTime);
    }

    public float GetSpeed()
    {
        return currentSpeed;
    }

    private void ApplyDeadzone(Joystick joystick, bool left)
    {
        float horizontal = 0;
        float vertical = 0;

        if (joystick.Horizontal >= controlDeadzone || joystick.Horizontal <= -controlDeadzone)
        {
            horizontal = joystick.Horizontal;
        }

        if (joystick.Vertical >= controlDeadzone || joystick.Vertical <= -controlDeadzone)
        {
            vertical = joystick.Vertical;
        }

        if (left)
        {
            ljsHorizontal = horizontal;
            ljsVertical = vertical;
        }
        else
        {
            rjsHorizontal = horizontal;
            rjsVertical = vertical;
        }
    }
}
