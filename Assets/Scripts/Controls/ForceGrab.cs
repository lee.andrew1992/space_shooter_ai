﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class ForceGrab : MonoBehaviour
{
    public LayerMask layers;
    public float grabbedPosition;
    public float grabDuration;
    public float grabRange;

    bool grabbing = false;
    float currGrabDuration;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
        {
            if (!grabbing)
            {
                if (!IsPointerOverUIObject())
                {
                    Ray raycast = MissionManager.Instance.playerTracker.GetPlayerCamera().ScreenPointToRay(Input.GetTouch(0).position);
                    RaycastHit raycastHit;
                    if (Physics.Raycast(raycast, out raycastHit, grabRange, layers))
                    {
                        EntityControllerAI entityAi;
                        if (UtilityFunctions.FindScriptInParent<EntityControllerAI>(raycastHit.transform.gameObject, raycastHit.transform.gameObject.layer, out entityAi))
                        {
                            currGrabDuration = 0;
                            grabbing = true;
                            StartCoroutine(Grab(entityAi));
                        }
                    }
                }
            }
        }
    }

    IEnumerator Grab(EntityControllerAI entity)
    {
        float lastSpeed = entity.DisableAi();
        GameObject grabbedTarget = entity.gameObject;

        while (grabbedTarget != null && currGrabDuration <= grabDuration)
        {
            this.GetComponent<ControlsShooting>().SetTarget(entity.GetComponent<AILockOnIndicator>());
            currGrabDuration += Time.deltaTime;

            Vector3 grabPosition = this.transform.position + (this.transform.forward * grabbedPosition);

            float grabSpeedMultiplier = Mathf.Max(Vector3.Distance(entity.gameObject.transform.position, grabPosition) / 10, 1);

            entity.gameObject.transform.position = 
                Vector3.Lerp(entity.gameObject.transform.position, grabPosition, Time.deltaTime * 2);

            yield return null;
        }

        if (grabbedTarget != null)
            entity.EnableAi(lastSpeed);
        grabbing = false;
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        List<RaycastResult> crossHairFilteredResults = new List<RaycastResult>();

        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject.tag != "Crosshair" && result.gameObject.tag != "Radar")
            {
                crossHairFilteredResults.Add(result);
            }
        }

        return crossHairFilteredResults.Count > 0;
    }
}
