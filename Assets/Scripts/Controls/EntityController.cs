﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController : MonoBehaviour
{
    public MissionManager.Layers currentTeam;

    [Header("HP")]
    public float maxHealth;
    public float currentHealth;
    public bool alive;

    [Header("Movement")]
    public float rotateSpeed;
    public float tiltSpeed;
    public float moveSpeedMax;
    public float moveSpeedMultiplier;
    public float crashDieSpeed;
    public float currentSpeed;

    [Header("Targetting")]
    public float targettingRange;

    [Header("Player Targetting")]
    public float targetLockOnLimit;
    public float targetLockOnThreshhold;

    void Start()
    {
        currentHealth = maxHealth;
        alive = true;

        MissionManager.Instance.enemiesTracker.AddShips(this.gameObject);
    }

    public virtual void TakeDamage(float damage)
    {
        switch (MissionManager.Instance.invincibility)
        {
            case MissionManager.Invincibility.Off:
                currentHealth -= damage;
                break;

            case MissionManager.Invincibility.Player:
                if (this.gameObject.layer != (int)MissionManager.Layers.PlayerInitial)
                {
                    currentHealth -= damage;
                }
                break;
            default:
                break;
        }

        if (this.gameObject == MissionManager.Instance.playerTracker.GetPlayerShip())
        {
            this.gameObject.GetComponent<CameraShake>().ShakeCam();
        }

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        alive = false;
        // Test Code
        if (this.gameObject == MissionManager.Instance.playerTracker.GetPlayerShip())
        {
        }
        else
        {
            MissionManager.Instance.playerTracker.GetPlayerShip().GetComponent<ControlsShooting>().ResetLockOnTimer();
            GameObject currentTarget = this.gameObject.GetComponent<AIMoveController>().currentTarget;

            if (currentTarget.GetComponent<AIMoveController>() != null)
            {
                currentTarget.GetComponent<AIMoveController>().currentPursuers.Remove(this.gameObject);
            }
            else
            {
                currentTarget.GetComponent<ControlsSetTarget>().pursuers.Remove(this.gameObject);
            }
        }
        MissionManager.Instance.enemiesTracker.HandleDeath(this.gameObject);
        Destroy(this.gameObject);   
    }
}
