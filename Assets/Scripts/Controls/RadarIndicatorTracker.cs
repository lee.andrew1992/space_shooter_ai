﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarIndicatorTracker : MonoBehaviour
{
    public GameObject trackingShip;

    GameObject playerShip;
    RectTransform indicator;
    Camera mainCam;

    private const float ANGLE_LIMIT_NORMAL = 20;
    private const float ANGLE_LIMIT_CAPITAL = 40;

    private float angleLimit;

    private bool started = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trackingShip)
        {

            if (trackingShip.activeSelf)
            {
                this.gameObject.transform.GetChild(0).gameObject.SetActive(true);

                Vector3 heading = trackingShip.transform.position - playerShip.transform.position;
                float angle = Vector3.Angle(heading, playerShip.transform.forward);

                if (angle < angleLimit)
                {
                    this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
                }
                else
                {
                    this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                    float directionX = Vector3.Dot(heading, playerShip.transform.right);
                    float directionY = Vector3.Dot(heading, playerShip.transform.up);

                    Vector2 direction = new Vector2(directionX, directionY).normalized;

                    this.gameObject.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = direction * 110;
                }
            }
            else
            {
                this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else
        {
            if (started)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
    }

    public void Instantiate(GameObject ship)
    {
        trackingShip = ship;

        if (trackingShip.GetComponent<EntityControllerAI>().shipType == EntityControllerAI.AIType.normal)
        {
            angleLimit = ANGLE_LIMIT_NORMAL;
        }
        else
        {
            angleLimit = ANGLE_LIMIT_CAPITAL;
        }
        playerShip = MissionManager.Instance.playerTracker.GetPlayerShip();
        mainCam = MissionManager.Instance.playerTracker.GetPlayerShip().GetComponentInChildren<Camera>();

        started = true;
    }
}
