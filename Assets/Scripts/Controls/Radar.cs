﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Radar : MonoBehaviour
{
    public GameObject radarBase;
    public GameObject normalAi;
    public GameObject capitalAi;

    [ColorUsageAttribute(true, true)]
    public Color enemyColor;
    [ColorUsageAttribute(true, true)]
    public Color allyColor;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Instantiate()
    {
        List<GameObject> enemies = MissionManager.Instance.enemiesTracker.GetEmpireShips();
        List<GameObject> allies = MissionManager.Instance.enemiesTracker.GetRebelShips();
        
        SpawnIndicators(enemies, enemyColor);
        SpawnIndicators(allies, allyColor);
    }

    void SpawnIndicators(List<GameObject> ships, Color indicatorColor)
    {
        foreach (GameObject ship in ships)
        {
            if (ship.GetComponent<EntityControllerAI>().shipType == EntityControllerAI.AIType.normal)
            {
                GameObject indicator = Instantiate(normalAi, radarBase.transform);
                indicator.GetComponent<RadarIndicatorTracker>().Instantiate(ship);

                Transform indicatorTick = indicator.transform.GetChild(0);
                indicatorTick.GetComponent<Image>().color = indicatorColor;
            }

            else
            {
                GameObject indicator = Instantiate(normalAi, radarBase.transform);
                indicator.GetComponent<RadarIndicatorTracker>().Instantiate(ship);

                Transform indicatorTick = indicator.transform.GetChild(0);
                indicatorTick.GetComponent<Image>().color = indicatorColor;
            }
        }
    }
}
