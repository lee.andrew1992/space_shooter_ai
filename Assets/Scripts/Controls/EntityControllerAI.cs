﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityControllerAI : EntityController
{
    [Header("General AI Values")]
    public float playerDetectionRange;
    public float detectionRange;
    public float maxTimePerAction;
    private Vector3 currentIntendedDestination;
    public GameObject currentBattle;
    public float currentBattleLimit;

    [Header("Shoot AI Values")]
    public float shotAngle;

    [Header("AI Dive Attack Related")]
    public float attractionEndDist;
    public float repulsionSlowDownFactor;
    public float repulsionNewForwardFactor;
    public float repulsionNewDirectionalFactor;

    [Header("AI Patrol Related")]
    public float patrolRandomDistMax;
    public float patrolRandomDistMin;

    [Header("AI Turn Around Related")]
    public float turnAroundValueMax;
    public float turnAroundValueMin;
    public float turnAroundSpeedReductionFactor;
    public float turnAroundRotateSpeedReductionFactor;

    [Header("AI Run Related")]
    public int runDirectionEnemyAverageMax;
    public float runDistance;
    public float runStartTime;

    [Header("AI Dodge Related")]
    public float dodgeDetectionRange;
    public float dodgeActivateSpeed;
    public float dodgeMagnitude;

    [Header("AI Return Related")]
    public float returnDistanceFactor;

    public AIType shipType;

    public enum AIType
    {
        normal,
        capital
    }

    void Start()
    {
        currentHealth = maxHealth;
        alive = true;
    }

    void Update()
    {
        if (MissionManager.Instance.debugMode && this.GetComponent<AIMoveController>() != null)
        {
            Color lineColor = Color.black;
            switch (this.GetComponent<AIMoveController>().currentState)
            {
                case AIMoveController.MoveState.Calculate:
                    lineColor = Color.blue;
                    break;

                case AIMoveController.MoveState.DiveAttacks:
                    lineColor = Color.red;
                    if (this.GetComponent<AIMoveController>().currentTarget == MissionManager.Instance.playerTracker.GetPlayerShip())
                        lineColor = Color.cyan;
                    break;

                case AIMoveController.MoveState.Dodge:
                    lineColor = Color.yellow;
                    break;

                case AIMoveController.MoveState.TurnAround:
                    lineColor = Color.green;
                    break;

                case AIMoveController.MoveState.Run:
                    lineColor = Color.white;
                    break;

                case AIMoveController.MoveState.Return:
                    lineColor = Color.gray;
                    break;
            }
            
            Debug.DrawLine(this.transform.position, currentIntendedDestination, lineColor, 0.05f);
        }
    }

    public void UpdateIntendedDestination(Vector3 newDestination)
    {
        currentIntendedDestination = newDestination;
    }

    public Vector3 CurrentDestination()
    {
        return currentIntendedDestination;
    }

    public float DisableAi()
    {
        AIMoveController aiMove = this.gameObject.GetComponent<AIMoveController>();
        UpdateIntendedDestination(this.transform.position);
        float lastSpeed = currentSpeed;
        currentSpeed = 0;
        aiMove.enabled = false;

        return lastSpeed;
    }

    public void EnableAi(float resetSpeed = 0)
    {
        AIMoveController aiMove = this.gameObject.GetComponent<AIMoveController>();
        aiMove.enabled = true;
        aiMove.ResetAi();
        currentSpeed = resetSpeed;
    }
}
