﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constant;

[RequireComponent(typeof(WeaponBase))]
public class ControlsShooting : MonoBehaviour
{
    private GameObject laserShotTracker;
    public WeaponBase equippedWeapon;
    public GameObject currentTarget = null;

    EntityController entityController;
    AILockOnIndicator currentLockOn;

    float currentShotCooldown;
    public float currentLockOnDuration;
    float maxLockOnDuration;
    float lockOnThreshhold;

    public LayerMask enemyLayerMask;

    public Joystick weaponJoystick;

    // Start is called before the first frame update
    void Start()
    {
        entityController = this.gameObject.GetComponent<EntityController>();
        maxLockOnDuration = entityController.targetLockOnLimit;
        lockOnThreshhold = entityController.targetLockOnThreshhold;

        laserShotTracker = MissionManager.Instance.miscTracker.LaserShotsTracker;
        currentShotCooldown = 0;

        if (entityController.currentTeam == MissionManager.Layers.Empire)
        {
            enemyLayerMask |= (1 << (int)MissionManager.Layers.RebellionTargetArea);
        }
        else
        {
            enemyLayerMask |= (1 << (int)MissionManager.Layers.EmpireTargetArea);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentShotCooldown > 0)
        {
            currentShotCooldown -= Time.deltaTime;
        }

        if (this.gameObject.layer == (int)MissionManager.Layers.PlayerInitial)
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, this.GetComponent<EntityController>().targettingRange, enemyLayerMask))
            {
                if (currentTarget == null)
                {
                    SetTarget(hit.transform.gameObject);
                }

                if (currentTarget == hit.transform.gameObject)
                {
                    currentLockOnDuration = Mathf.Min(currentLockOnDuration + Time.deltaTime, maxLockOnDuration);
                }
            }
            else
            {
                currentLockOnDuration = Mathf.Max(currentLockOnDuration - Time.deltaTime, 0);
            }

            if (weaponJoystick.Horizontal != 0 || weaponJoystick.Vertical != 0)
            {
                if (currentTarget != null && currentLockOnDuration >= lockOnThreshhold)
                    Shoot(currentTarget.transform.gameObject);
                else
                    Shoot();
            }

            if (currentTarget != null)
            {
                currentLockOn.ActivateLockOn(currentLockOnDuration, lockOnThreshhold);

                if (currentLockOnDuration == 0)
                {
                    currentLockOn.DeactivateLockOn();
                    currentTarget = null;
                }
            }
        }
    }

    public void Shoot(GameObject target = null)
    {
        if (currentShotCooldown <= 0)
        {
            equippedWeapon.Shoot(laserShotTracker, target);
            currentShotCooldown = equippedWeapon.laserShotCooldown;
        }
    }

    public void ResetLockOnTimer()
    {
        currentLockOnDuration = 0;
    }

    public void SetTarget(GameObject target)
    {
        currentTarget = target;
        UtilityFunctions.FindScriptInParent<AILockOnIndicator>(currentTarget, currentTarget.layer, out currentLockOn);
    }

    public void SetTarget(AILockOnIndicator aiLockOn)
    {
        currentTarget = aiLockOn.gameObject;
        currentLockOn = aiLockOn;
    }
}
