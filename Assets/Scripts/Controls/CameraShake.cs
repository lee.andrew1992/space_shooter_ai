﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Camera mainCamera;

    public float shakeDuration;
    public float shakeMagnitudeMax;
    public float shakeMagnitudeMin;
    public float dampingSpeed;
    public Vector3 initialPosition;

    private float currentShakeMagnitude;
    private float currentShakeDuration;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = mainCamera.transform.localPosition;
        currentShakeMagnitude = shakeMagnitudeMax;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            ShakeCam();
        }

        if (currentShakeDuration > 0)
        {
            mainCamera.transform.localPosition = initialPosition + Random.insideUnitSphere * currentShakeMagnitude;
            currentShakeMagnitude = Mathf.Max(currentShakeMagnitude - (Time.deltaTime * dampingSpeed), shakeMagnitudeMin);
            currentShakeDuration -= Time.deltaTime * dampingSpeed;
        }

        else
        {
            currentShakeDuration = 0;
            mainCamera.transform.localPosition = initialPosition;
        }
    }

    public void ShakeCam()
    {
        currentShakeDuration = shakeDuration;
        currentShakeMagnitude = shakeMagnitudeMax; 
    }
}
