﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBase : MonoBehaviour
{
    public GameObject firePoints;
    public GameObject laserShotPrefab;

    public float laserShotSpeed;
    public float laserShotDuration;
    public float laserShotCooldown;
    public float damage;
    public bool alternateShots;
    public Material laserShotColor;

    int shotIndex = 0;

    void Start()
    {
    }

    public void Shoot(GameObject laserShotTracker, GameObject target = null)
    {
        List<GameObject> laserShots = new List<GameObject>();
        Quaternion aimAngle = this.transform.rotation;

        if (target != null)
        {
            Vector3 direction = target.transform.position + target.transform.forward - this.transform.position;
            aimAngle = Quaternion.LookRotation(direction, Vector3.up);
        }

        if (alternateShots)
        {
            GameObject shot = GameObject.Instantiate(laserShotPrefab, firePoints.transform.GetChild(shotIndex).position, aimAngle, laserShotTracker.transform);   

            firePoints.transform.GetChild(shotIndex).GetComponent<AudioSource>().Play();

            laserShots.Add(shot);
            shotIndex++;

            if (shotIndex >= firePoints.transform.childCount)
            {
                shotIndex = 0;
            }
        }

        else
        {
            foreach (Transform firePoint in firePoints.transform)
            {
                GameObject shot = GameObject.Instantiate(laserShotPrefab, firePoint.transform.position, aimAngle, laserShotTracker.transform);

                laserShots.Add(shot);
            }
        }

        foreach (GameObject shot in laserShots)
        {
            Vector3 eulerAngles = shot.transform.rotation.eulerAngles;
            shot.transform.rotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
            shot.GetComponent<LaserShot>().Instantiate(laserShotSpeed, laserShotDuration, laserShotCooldown, damage, this.gameObject.layer, laserShotColor, target);
        }
    }
}
