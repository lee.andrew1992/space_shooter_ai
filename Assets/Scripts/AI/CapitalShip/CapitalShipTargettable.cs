﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapitalShipTargettable : EntityController
{
    public float maxHp;
    public float currentHp;

    public GameObject [] followDeathObjects;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Die()
    {
        foreach (GameObject followDeathObject in followDeathObjects)
        {
            Destroy(followDeathObject);
        } 

        base.Die();
    }
}
