﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityControllerCapitalAI : EntityControllerAI
{
    [Header("Capital Ship AI Values")]
    public CapitalShipTargettable shieldBattery;
    public CapitalShipTargettable innerWall;
    public CapitalShipTargettable powerGenerator;
    public CapitalShipTargettable escapePath;

    public CapitalShipTargettable [] turrets;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void TakeDamage(float damage)
    {
        // do nothing as the main ship will not take damage
    }
}
