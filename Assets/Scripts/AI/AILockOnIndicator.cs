﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILockOnIndicator : MonoBehaviour
{
    public GameObject targettingArea;

    [ColorUsageAttribute(true, true)]
    public Color lockingOnColor;
    [ColorUsageAttribute(true, true)]
    public Color lockedOnColor;


    private float targettedDuration = 0;
    private float threshhold = 0;

    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer renderer = targettingArea.GetComponent<MeshRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        LockOnShow(targettedDuration, threshhold);
    }

    public void ActivateLockOn(float duration, float lockOnThreshhold)
    {
        targettedDuration = duration;
        threshhold = lockOnThreshhold;
    }

    public void DeactivateLockOn()
    {
        targettedDuration = 0;
        threshhold = 0;
    }

    private void LockOnShow(float duration, float lockOnThreshhold)
    {
        MeshRenderer renderer = targettingArea.GetComponent<MeshRenderer>();

        if (duration < lockOnThreshhold)
        {
            renderer.material.SetColor("_MainColor", lockingOnColor * (duration));
        }
        else
        {
            renderer.material.SetColor("_MainColor", lockedOnColor * (duration * 2));
        }

    }
}
