﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ControlsShooting))]
public class AIShoot : MonoBehaviour
{
    public GameObject firePoints;
    public ControlsShooting controlsShooting;
    public float attackRange;
    public LayerMask attackTargetLayers;
    private List<GameObject> enemies;

    private TrackerEnemies enemyTracker;

    float firePointsY;
    float currentAttackTimer;

    // Start is called before the first frame update
    void Start()
    {
        firePointsY = firePoints.transform.position.y;
        enemyTracker = MissionManager.Instance.enemiesTracker;

        if (this.gameObject.layer == enemyTracker.teamEmpire.layer)
        {
            enemies = enemyTracker.GetRebelShips();
        }
        else
        {
            enemies = enemyTracker.GetEmpireShips();
        }
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(this.transform.position.x, firePointsY, this.transform.position.z), this.transform.forward, out hit, attackRange, attackTargetLayers))
        {
            StartCoroutine(ShootForDuration(0.2f));
        }
    }

    private IEnumerator ShootForDuration(float duration)
    {
        while (currentAttackTimer < duration)
        {
            controlsShooting.Shoot();
            currentAttackTimer += Time.deltaTime;
            yield return null;
        }

        currentAttackTimer = 0;
    }
}
