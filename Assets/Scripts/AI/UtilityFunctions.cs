﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public static class UtilityFunctions
{
    public static bool IsInFront(GameObject origin, GameObject target)
    {
        return IsInFront(origin, target.transform.position);
    }

    public static bool IsInFront(GameObject origin, Vector3 targetPosition)
    {
        Vector3 heading = targetPosition - origin.transform.position;
        float dot = Vector3.Dot(heading, origin.transform.forward);

        return dot > 0;
    }

    public static bool IsInFront(GameObject origin, GameObject target, float radiansAngle)
    {
        return IsInFront(origin, target.transform.position, radiansAngle);
    }

    public static bool IsInFront(GameObject origin, Vector3 targetPosition, float radiansAngle)
    {
        Vector3 heading = targetPosition - origin.transform.position;
        float dot = Vector3.Dot(heading, origin.transform.forward);

        if (dot > 0)
        {
            float angle = Vector3.Angle(heading, origin.transform.forward);

            return angle < radiansAngle;
        }
        else
        {
            return false;
        }
    }

    public static List<GameObject> GetSortedShips(List<GameObject> ships, GameObject originShip)
    {
        if (ships.Count <= 1)
        {
            return ships;
        }

        List<GameObject> sortedShips = new List<GameObject>();
        sortedShips = ships.OrderBy(
            x => Vector3.Distance(originShip.transform.position, x.transform.position)
        ).ToList();

        return sortedShips;
    }

    public static bool FindScriptInParent<T>(GameObject origin, int layer, out T script)
    {
        script = origin.GetComponent<T>();

        if (script != null)
            return true;

        if (origin.layer != layer)
            return false;
        else
            try
            {
                return FindScriptInParent<T>(origin.transform.parent.gameObject, layer, out script);
            }
            catch (NullReferenceException)
            {
                return false;
            }
    }
}
