﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AIMoveDiveAttacks))]
[RequireComponent(typeof(AIMovePatrol))]
[RequireComponent(typeof(AIMoveRun))]
[RequireComponent(typeof(AIMoveCalculate))]
[RequireComponent(typeof(AIMoveTurnAround))]
[RequireComponent(typeof(AIMoveReturn))]
public class AIMoveController : MonoBehaviour
{
    public GameObject currentTarget;
    public float currentTargetDuration;
    public List<GameObject> currentPursuers = new List<GameObject>();

    public enum MoveState
    {
        Calculate,
        Patrol,
        DiveAttacks,
        Return,
        TurnAround,
        Dodge,
        Run
    }

    public MoveState currentState;

    private MoveState _lastState;
    public MoveState lastState { get { return _lastState; } set { _lastState = value; } }

    private AIMoveCalculate calculate;
    private AIMoveDiveAttacks diveAttacks;
    private AIMovePatrol patrol;
    private AIMoveTurnAround turnAround;
    private AIMoveRun runAway;
    private AIMoveReturn returnBack;

    private EntityControllerAI entityController;

    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();

        currentState = MoveState.Calculate;
        _lastState = MoveState.Calculate;
        currentTarget = null;

        calculate = this.GetComponent<AIMoveCalculate>();
        diveAttacks = this.GetComponent<AIMoveDiveAttacks>();
        patrol = this.GetComponent<AIMovePatrol>();
        turnAround = this.GetComponent<AIMoveTurnAround>();
        runAway = this.GetComponent<AIMoveRun>();
        returnBack = this.GetComponent<AIMoveReturn>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTarget != null && Vector3.Distance(this.transform.position, currentTarget.transform.position) > entityController.detectionRange)
        {
            currentTarget = null;
        }

        switch (currentState)
        {
            case MoveState.Calculate:
                calculate.Enable(true);
                break;

            case MoveState.Patrol:
                patrol.Enable();
                break;

            case MoveState.DiveAttacks:
                diveAttacks.Enable();
                break;

            case MoveState.Return:
                returnBack.Enable();
                break;

            case MoveState.TurnAround:
                turnAround.Enable();
                break;

            case MoveState.Run:
                runAway.Enable();
                break;

            case MoveState.Dodge:
                break;
        }
    }

    public void SetTarget(GameObject target)
    {
        if (currentTarget != null)
        {
            LoseTarget();
        }
        currentTarget = target;

        if (target == MissionManager.Instance.playerTracker.GetPlayerShip())
        {
            target.GetComponent<ControlsSetTarget>().pursuers.Add(this.gameObject);
        }
        else
        {
            target.GetComponent<AIMoveController>().currentPursuers.Add(this.gameObject);
        }

    }

    public void LoseTarget()
    {
        if (currentTarget == MissionManager.Instance.playerTracker.GetPlayerShip())
        {
            currentTarget.GetComponent<ControlsSetTarget>().pursuers.Remove(this.gameObject);
        }
        else
        {
            currentTarget.GetComponent<AIMoveController>().currentPursuers.Remove(this.gameObject);
        }
        currentTarget = null;
    }

    public void ResetAi()
    {
        switch (currentState)
        {
            case MoveState.Calculate:
                calculate.Disable();
                break;

            case MoveState.Patrol:
                patrol.Disable();
                break;

            case MoveState.DiveAttacks:
                diveAttacks.Disable();
                break;

            case MoveState.Return:
                returnBack.Disable();
                break;

            case MoveState.TurnAround:
                turnAround.Disable();
                break;

            case MoveState.Run:
                runAway.Disable();
                break;

            case MoveState.Dodge:
                break;
        }
    }
}
