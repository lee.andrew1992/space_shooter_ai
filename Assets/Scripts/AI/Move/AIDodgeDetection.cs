﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDodgeDetection : MonoBehaviour
{
    AIMoveDodge moveDodge = null;
    AIMoveController aiMoveController;
    EntityControllerAI entityController;

    // Start is called before the first frame update
    void Start()
    {
        aiMoveController = this.transform.parent.gameObject.GetComponent<AIMoveController>();
        entityController = this.transform.parent.gameObject.GetComponent<EntityControllerAI>();
    }

    // Update is called once per frame
    void Update()
    {   
    }

    public void Initialize(AIMoveDodge aiScript)
    {
        moveDodge = aiScript;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (moveDodge != null)
        {
            if (other.gameObject.layer == (int)MissionManager.Layers.Empire ||
                other.gameObject.layer == (int)MissionManager.Layers.Rebellion ||
                other.gameObject.layer == (int)MissionManager.Layers.PlayerInitial)
            {
                RaycastHit hit;
                //if (Physics.Raycast(this.transform.position, entityController.CurrentDestination(), out hit, entityController.dodgeDetectionRange, other.gameObject.layer))
                if (Physics.Raycast(this.transform.position, transform.forward, out hit, entityController.dodgeDetectionRange, other.gameObject.layer))
                {
                        Debug.Log("Have to dodge!!!!");
                }
            }

            else if (other.gameObject.layer == (int)MissionManager.Layers.Capital)
            {
                // Immovable Capital ship is too close need to dodge
                moveDodge.SetPositionToDodge(other.ClosestPoint(this.gameObject.transform.position));
                moveDodge.Enable();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == (int)MissionManager.Layers.Capital)
        {
            moveDodge.StopDodge();
        }
    }
}
