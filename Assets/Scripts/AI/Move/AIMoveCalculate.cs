﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveCalculate : AIMoveBase
{
    // Start is called before the first frame update
    void Start()
    {
        aiMoveController = GetComponent<AIMoveController>();
        entityController = GetComponent<EntityControllerAI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            currentDirection = TurnDirection.Straight;
            Move();
            FaceDirection();

            if (TooFarFromBattle())
            {
                SetNewState(AIMoveController.MoveState.Return);
            }

            else
            {
                if (aiMoveController.currentTarget != null && Vector3.Distance(aiMoveController.currentTarget.transform.position, this.transform.position) < entityController.detectionRange)
                {
                    float pursuedForSum = 0;
                    if (aiMoveController.currentPursuers.Count > 0)
                    {
                        foreach (GameObject pursuer in aiMoveController.currentPursuers)
                        {
                            if (pursuer == null)
                            {
                                aiMoveController.currentPursuers.Remove(pursuer);
                            }
                            else
                            {
                                if (pursuer.GetComponent<AIMoveController>() != null)
                                {
                                    pursuedForSum += pursuer.GetComponent<AIMoveController>().currentTargetDuration;
                                }
                                else
                                {
                                    pursuedForSum += pursuer.GetComponent<ControlsShooting>().currentLockOnDuration;
                                }
                            }
                        }
                    }

                    if (pursuedForSum > entityController.runStartTime && aiMoveController.lastState != AIMoveController.MoveState.Run)
                    {
                        pursuedForSum = 0;
                        foreach (GameObject pursuer in aiMoveController.currentPursuers)
                        {
                            if (pursuer.layer != (int)MissionManager.Layers.PlayerInitial)
                            {
                                pursuer.GetComponent<AIMoveController>().currentTargetDuration = 0;
                            }
                        }
                        SetNewState(AIMoveController.MoveState.Run);
                    }
                    else
                    {
                        if (UtilityFunctions.IsInFront(this.gameObject, aiMoveController.currentTarget))
                        {
                            SetNewState(AIMoveController.MoveState.DiveAttacks);
                        }
                        else
                        {
                            aiMoveController.currentTargetDuration = 0;
                            SetNewState(AIMoveController.MoveState.TurnAround);
                        }
                    }
                }
                else if (aiMoveController.currentTarget == null && aiMoveController.currentPursuers.Count > 0)
                {
                    List<GameObject> sortedPursuers = UtilityFunctions.GetSortedShips(aiMoveController.currentPursuers, this.gameObject);
                    aiMoveController.currentTarget = sortedPursuers[0];
                }
                else
                {
                    if (aiMoveController.currentTarget != null)
                    {
                        aiMoveController.LoseTarget();
                    }
                    SetNewState(AIMoveController.MoveState.Patrol);
                }
            }

        }   
    }

    private void SetNewState(AIMoveController.MoveState newState)
    {
        aiMoveController.lastState = aiMoveController.currentState;
        aiMoveController.currentState = newState;
        this.activate = false;
    }

    protected override void AddActions()
    {
    }

    bool TooFarFromBattle()
    {
        if (Vector3.Distance(this.transform.position, entityController.currentBattle.transform.position) > entityController.currentBattleLimit)
        {
            return true;
        }
        return false;
    }
}
