﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveReturn : AIMoveBase
{
    // Start is called before the first frame update
    void Start()
    {
        aiMoveController = GetComponent<AIMoveController>();
        entityController = GetComponent<EntityControllerAI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            FaceDirection();
            Move();
        }
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(Return());
    }

    private IEnumerator Return()
    {
        currentDirection = TurnDirection.Straight;
        entityController.UpdateIntendedDestination(entityController.currentBattle.transform.position);

        while (aiMoveController.currentTarget != null &&
            Vector3.Distance(this.transform.position, entityController.CurrentDestination()) > entityController.currentBattleLimit / entityController.returnDistanceFactor)
        {
            MatchSpeed(entityController.moveSpeedMax);
            currentActionTime += Time.deltaTime;
            yield return null;
        }

        currentActionTime = 0;
    }
}
