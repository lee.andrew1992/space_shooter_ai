﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveDodge : AIMoveBase
{
    public GameObject collideDetector;
    private Vector3 positionToDodge;

    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();
        aiMoveController = this.GetComponent<AIMoveController>();
        collideDetector.transform.localScale = new Vector3(entityController.dodgeDetectionRange, entityController.dodgeDetectionRange, entityController.dodgeDetectionRange);
        collideDetector.GetComponent<AIDodgeDetection>().Initialize(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPositionToDodge(Vector3 position)
    {
        positionToDodge = position;
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(QuickDodge());
    }

    IEnumerator QuickDodge()
    {
        Vector3 heading = this.transform.position - positionToDodge;
        Vector3 direction = heading / heading.magnitude;

        entityController.UpdateIntendedDestination(this.transform.position + (this.transform.forward * entityController.dodgeDetectionRange) + (direction * entityController.dodgeMagnitude));

        while (currentActionTime < entityController.maxTimePerAction &&
            Vector3.Distance(this.gameObject.transform.position, entityController.CurrentDestination()) > 1)
        {
            currentActionTime += Time.deltaTime;
            MatchSpeed(entityController.moveSpeedMax / 3f);
            yield return null;
        }

        currentActionTime = 0;
    }

    public override void Enable(bool isCalculate = false)
    {
        if (aiMoveController.currentState != AIMoveController.MoveState.Dodge)
        {
            actionQueue.Clear();
            aiMoveController.currentState = AIMoveController.MoveState.Dodge;
        }

        base.Enable();
    }

    public void StopDodge()
    {
        if (aiMoveController.currentState == AIMoveController.MoveState.Dodge)
        {
            actionQueue.Clear();
            Disable();
        }
    }

    public override void Disable()
    {
        positionToDodge = this.transform.position;
        base.Disable();
    }
}
