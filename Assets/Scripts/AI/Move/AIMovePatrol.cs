﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovePatrol : AIMoveBase
{
    private List<GameObject> enemyTeam;

    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();
        aiMoveController = GetComponent<AIMoveController>();
        currentDirection = TurnDirection.Straight;

        if (this.gameObject.layer == (int)MissionManager.Layers.Empire)
            enemyTeam = MissionManager.Instance.enemiesTracker.GetRebelShips();
        else
            enemyTeam = MissionManager.Instance.enemiesTracker.GetEmpireShips();
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            FaceDirection();
            Move();
        }

        if (aiMoveController.currentTarget == null || 
            Vector3.Distance(aiMoveController.currentTarget.transform.position, this.transform.position) > entityController.detectionRange)
        {
            List<GameObject> closestEnemies = UtilityFunctions.GetSortedShips(enemyTeam, this.gameObject);

            GameObject playerShip = MissionManager.Instance.playerTracker.GetPlayerShip();

            if (playerShip != null && (int)playerShip.GetComponent<EntityController>().currentTeam != this.gameObject.layer &&
                playerShip.GetComponent<ControlsSetTarget>().pursuers.Count < 3 &&
                closestEnemies.IndexOf(playerShip) < 3 && 
                Vector3.Distance(playerShip.transform.position, this.transform.position) < entityController.playerDetectionRange)
            {
                aiMoveController.SetTarget(playerShip);
            }
            else
            {
                foreach (GameObject enemy in closestEnemies)
                {
                    if (enemy.tag != "CapitalShip" && (Vector3.Distance(this.transform.position, enemy.transform.position) < entityController.detectionRange)
                        && UtilityFunctions.IsInFront(this.gameObject, enemy))
                    {
                        aiMoveController.SetTarget(enemy);
                        break;
                    }
                }
            }
        }
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(MoveRandom());
    }

    private IEnumerator MoveRandom()
    {
        Vector3 intendedMovePath =
            this.transform.forward * Random.Range(entityController.patrolRandomDistMin, entityController.patrolRandomDistMax) * (Random.Range(-1, 2)) +
            this.transform.up * Random.Range(entityController.patrolRandomDistMin, entityController.patrolRandomDistMax) * (Random.Range(-1, 2)) +
            this.transform.right * Random.Range(entityController.patrolRandomDistMin, entityController.patrolRandomDistMax) * (Random.Range(-1, 2));

        entityController.UpdateIntendedDestination(this.transform.position + intendedMovePath);

        while (currentActionTime < entityController.maxTimePerAction && 
            Vector3.Distance(this.gameObject.transform.position, entityController.CurrentDestination()) > 1)
        {
            currentActionTime += Time.deltaTime;
            MatchSpeed(entityController.moveSpeedMax / 1.5f);
            yield return null;
        }

        currentActionTime = 0;
    }
}
