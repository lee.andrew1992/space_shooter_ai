﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityControllerAI))]
public abstract class AIMoveBase : MonoBehaviour
{
    protected bool activate;
    protected EntityControllerAI entityController;
    protected AIMoveController aiMoveController;
    protected float currentActionTime;
    protected List<GameObject> pursuers = new List<GameObject>();

    protected static Queue<IEnumerator> actionQueue = new Queue<IEnumerator>();

    protected enum TurnDirection
    {
        Straight = -1,
        Left = 0,
        Right = 1,
        Up = 2,
        Down = 3
    }

    protected static TurnDirection currentDirection;

    // Update is called once per frame
    void Update()
    {
    }

    public virtual void Enable(bool isCalculate = false)
    {
        if (!activate)
        {
            activate = true;
            if (!isCalculate)
            {
                AddActions();
                this.StopAllCoroutines();
                StartCoroutine(ExecuteActions());
            }
        }
    }

    public virtual void Disable()
    {
        activate = false;
        aiMoveController.currentState = AIMoveController.MoveState.Calculate;
    }

    protected void FaceDirection()
    {
        FaceDirection(entityController.rotateSpeed);
    }

    protected void FaceDirection(float rotateSpeed)
    {
        if (entityController.CurrentDestination() != transform.position)
        {
            var targetRotation = Quaternion.LookRotation(entityController.CurrentDestination() - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotateSpeed);
        }
    }

    protected void Move()
    {
        if (entityController.CurrentDestination() != transform.position)
        {
            transform.position += transform.forward * Time.deltaTime * entityController.currentSpeed;
        }
    }

    protected void Accelerate(float maxSpeed)
    {
        if (maxSpeed > entityController.moveSpeedMax)
            maxSpeed = entityController.moveSpeedMax;

        entityController.currentSpeed = Mathf.Min(entityController.currentSpeed + (Time.deltaTime * entityController.moveSpeedMultiplier), maxSpeed);
    }

    protected void Decelerate(float minSpeed)
    {
        if (minSpeed < 0)
            minSpeed = 0;

        if (entityController.currentSpeed < minSpeed)
        {
            entityController.currentSpeed = minSpeed;
        }
        else
        {
            entityController.currentSpeed = entityController.currentSpeed - Time.deltaTime * entityController.moveSpeedMultiplier;
        }
    }

    protected void MatchSpeed(float speed)
    {
        if (entityController.currentSpeed > speed)
        {
            Decelerate(speed);
        }
        else
        {
            Accelerate(speed);
        }
    }

    protected abstract void AddActions();
    protected IEnumerator ExecuteActions()
    {
        while (actionQueue.Count > 0)
        {
            yield return actionQueue.Dequeue();
        }

        Disable();
    }
}
