﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveTurnAround : AIMoveBase
{
    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();
        aiMoveController = GetComponent<AIMoveController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            FaceDirection(entityController.rotateSpeed / entityController.turnAroundSpeedReductionFactor);
            Move();
        }
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(Turn());
    }

    private IEnumerator Turn()
    {
        if (aiMoveController.currentTarget != null)
        {
            currentDirection = (TurnDirection)Random.Range(0, 4);

            entityController.UpdateIntendedDestination(this.transform.position - this.transform.forward * 100);

            Vector3 newDirection = Vector3.zero;

            switch (currentDirection)
            {
                case TurnDirection.Right:
                    newDirection = (this.transform.right * Random.Range(entityController.turnAroundValueMin, entityController.turnAroundValueMax));
                    break;

                case TurnDirection.Left:
                    newDirection = (this.transform.right * -Random.Range(entityController.turnAroundValueMin, entityController.turnAroundValueMax));
                    break;

                case TurnDirection.Up:
                    newDirection = (this.transform.up * Random.Range(entityController.turnAroundValueMin, entityController.turnAroundValueMax));
                    break;

                case TurnDirection.Down:
                    newDirection = (this.transform.up * -Random.Range(entityController.turnAroundValueMin, entityController.turnAroundValueMax));
                    break;
            }

            entityController.UpdateIntendedDestination(entityController.CurrentDestination() + newDirection);

            while (aiMoveController.currentTarget != null &&
                currentActionTime < entityController.maxTimePerAction && 
                !UtilityFunctions.IsInFront(this.gameObject, entityController.CurrentDestination(), 90))
            {
                currentActionTime += Time.deltaTime;
                MatchSpeed(entityController.moveSpeedMax / entityController.turnAroundSpeedReductionFactor);
                yield return null;
            }

            GameObject playerShip = MissionManager.Instance.playerTracker.GetPlayerShip();

            if (playerShip != null && aiMoveController.currentTarget != playerShip)
            {
                if (playerShip.GetComponent<ControlsSetTarget>().pursuers.Count < 3 &&
                    Vector3.Distance(playerShip.transform.position, this.transform.position) < entityController.playerDetectionRange &&
                    UtilityFunctions.IsInFront(this.gameObject, playerShip, 60) &&
                    (int)playerShip.GetComponent<EntityController>().currentTeam != this.gameObject.layer)
                {
                    aiMoveController.SetTarget(playerShip);
                }
            }
        }

        currentActionTime = 0;
    }
}
