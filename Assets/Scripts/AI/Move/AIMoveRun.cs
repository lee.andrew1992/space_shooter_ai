﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveRun : AIMoveBase
{
    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();
        aiMoveController = GetComponent<AIMoveController>();
        currentDirection = TurnDirection.Straight;
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            FaceDirection();
            Move();
        }
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(MoveStraight());
    }

    private IEnumerator MoveStraight()
    {
        List<GameObject> closestPursuers = UtilityFunctions.GetSortedShips(aiMoveController.currentPursuers, this.gameObject);
        int currentMax = Mathf.Min(closestPursuers.Count, entityController.runDirectionEnemyAverageMax);

        Vector3 newDirection = this.transform.forward;


        entityController.UpdateIntendedDestination(this.transform.position + (newDirection * entityController.runDistance));

        while (Vector3.Distance(this.transform.position, entityController.CurrentDestination()) > 50 )
        {
            MatchSpeed(entityController.moveSpeedMax);
            currentActionTime += Time.deltaTime;
            yield return null;
        }

        currentActionTime = 0;
    }
}
