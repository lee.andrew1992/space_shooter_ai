﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMoveDiveAttacks : AIMoveBase
{
    // Start is called before the first frame update
    void Start()
    {
        entityController = this.GetComponent<EntityControllerAI>();
        aiMoveController = GetComponent<AIMoveController>();
        currentDirection = TurnDirection.Straight;
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            aiMoveController.currentTargetDuration += Time.deltaTime;
            FaceDirection();
            Move();

            if (aiMoveController.currentTarget == null)
            {
                actionQueue.Clear();
                Disable();
            }
        }
    }

    protected override void AddActions()
    {
        actionQueue.Enqueue(MoveIn());
        actionQueue.Enqueue(SlideOut());
    }

    private IEnumerator MoveIn()
    {
        // attraction
        currentDirection = TurnDirection.Straight;
        while (aiMoveController.currentTarget != null &&
            UtilityFunctions.IsInFront(this.gameObject, aiMoveController.currentTarget) &&
            currentActionTime < entityController.maxTimePerAction && 
            Vector3.Distance(this.gameObject.transform.position, aiMoveController.currentTarget.transform.position) > entityController.attractionEndDist)
        {
            entityController.UpdateIntendedDestination(aiMoveController.currentTarget.transform.position);
            MatchSpeed(entityController.moveSpeedMax);
            currentActionTime += Time.deltaTime;
            yield return null;
        }

        currentActionTime = 0;
    }

    private IEnumerator SlideOut()
    {
        // repulsion
        if (aiMoveController.currentTarget != null)
        {
            currentDirection = (TurnDirection)Random.Range(0, 4);
            Vector3 outwardsDirection = Vector3.zero;

            switch (currentDirection)
            {
                case TurnDirection.Down:
                    outwardsDirection = (this.transform.forward * entityController.repulsionNewForwardFactor) -
                            (this.transform.up * entityController.repulsionNewDirectionalFactor);
                    break;

                case TurnDirection.Up:
                    outwardsDirection = (this.transform.forward * entityController.repulsionNewForwardFactor) +
                            (this.transform.up * entityController.repulsionNewDirectionalFactor);
                    break;

                case TurnDirection.Left:
                    outwardsDirection = (this.transform.forward * entityController.repulsionNewForwardFactor) -
                            (this.transform.right * entityController.repulsionNewDirectionalFactor);
                    break;

                case TurnDirection.Right:
                    outwardsDirection = (this.transform.forward * entityController.repulsionNewForwardFactor) +
                            (this.transform.right * entityController.repulsionNewDirectionalFactor);
                    break;
            }

            entityController.UpdateIntendedDestination(aiMoveController.currentTarget.transform.position + outwardsDirection);

            while (this.gameObject != null &&
                Vector3.Distance(this.gameObject.transform.position, entityController.CurrentDestination()) > entityController.attractionEndDist)
            {
                currentActionTime += Time.deltaTime;
                Accelerate(entityController.moveSpeedMax);
                yield return null;
            }
            
        }

        currentActionTime = 0;
        currentDirection = TurnDirection.Straight;
    }
}
