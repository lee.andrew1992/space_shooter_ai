﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLogic : MonoBehaviour
{
    public GameObject terrain;
    public GameObject weather;
    public float fogDensity;
    public bool fogEnabled;
    public Color fogColor;

    public Material skybox;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void EnterLevel()
    {
        if (terrain)
            MissionManager.Instance.currTerrain = GameObject.Instantiate(terrain);
        else
        {
            GameObject.Destroy(MissionManager.Instance.currTerrain);
            MissionManager.Instance.currTerrain = null;
        }

        if (weather)
        {
            MissionManager.Instance.currWeather = GameObject.Instantiate(weather);
            MissionManager.Instance.currWeather.GetComponent<EmitterPositionController>().PlayerOrCamera = MissionManager.Instance.playerTracker.GetPlayerShip().transform.GetChild(0);
        }
        else
        {
            GameObject.Destroy(MissionManager.Instance.currWeather);
            MissionManager.Instance.currWeather = null;
        }

        RenderSettings.skybox = this.skybox;
        RenderSettings.fog = fogEnabled;
        RenderSettings.fogDensity = this.fogDensity;
        RenderSettings.fogColor = this.fogColor;
    }
}
