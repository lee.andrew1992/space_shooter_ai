﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSetup : MonoBehaviour
{
    [Header("Empire")]
    public GameObject empireShip;
    public GameObject empireCapital;
    public int empireShipsCount;
    public int empireCapitalCount;

    [Header("Rebellion")]
    public GameObject rebellionShip;
    public GameObject rebellionCapital;
    public int rebellionShipsCount;
    public int rebellionCapitalCount;

    [Header("Distance")]
    public float initialSideDistance;
    public float shipRowDistance;
    public float additionalCaptialDistance;
    public float capitalRowDistance;

    private Vector3 center;

    // Start is called before the first frame update
    void Start()
    {
        center = this.transform.position;
    }

    IEnumerator SpawnShips(GameObject shipObject, int count)
    {
        for (int index = 0; index < count; index++)
        {
            Vector3 spawnPos = Vector3.zero;

            if (shipObject.gameObject.layer == (int)MissionManager.Layers.Empire)
            {
                spawnPos = center - (new Vector3(shipRowDistance * index, 0, initialSideDistance / 2));
            }
            else
            {
                spawnPos = center + (new Vector3(shipRowDistance * index, 0, initialSideDistance / 2));
            }

            GameObject ship = Instantiate(shipObject, spawnPos, Quaternion.LookRotation(center));
            ship.GetComponent<EntityControllerAI>().currentBattle = this.gameObject;
            MissionManager.Instance.enemiesTracker.AddShips(ship);
        }
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator SpawnCapitalShips(GameObject capitalShip, int count)
    {
        for (int index = 0; index < count; index++)
        {
            Vector3 spawnPos = Vector3.zero;

            if (capitalShip.gameObject.layer == (int)MissionManager.Layers.Empire)
            {
                spawnPos = center - (new Vector3(capitalRowDistance * index, 0, additionalCaptialDistance + (initialSideDistance / 2)));
            }
            else
            {
                spawnPos = center + (new Vector3(capitalRowDistance * index, 0, additionalCaptialDistance + (initialSideDistance / 2)));
            }

            GameObject ship = Instantiate(capitalShip, spawnPos, Quaternion.LookRotation(center));
            ship.GetComponent<EntityControllerCapitalAI>().currentBattle = this.gameObject;
            MissionManager.Instance.enemiesTracker.AddShips(ship);
        }
        yield return new WaitForSeconds(0.5f);
    }

    public void InitiateBattle()
    {
        StartCoroutine(SpawnShips(rebellionShip, rebellionShipsCount));
        StartCoroutine(SpawnCapitalShips(rebellionCapital, rebellionCapitalCount));

        StartCoroutine(SpawnShips(empireShip, empireShipsCount));
        StartCoroutine(SpawnCapitalShips(empireCapital, empireCapitalCount));
    }
}
