﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelChanger : MonoBehaviour
{
    public enum SceneId
    {
        SpaceScene = 0,
        PlanetScene = 1
    }

    private Camera playerCam;

    public void ChangeLevel(int levelToLoad)
    {
        if (playerCam == null)
        {
            playerCam = MissionManager.Instance.playerTracker.GetPlayerShip().transform.GetChild(0).GetComponent<Camera>();
        }

        foreach (Transform laser in MissionManager.Instance.miscTracker.LaserShotsTracker.transform)
        {
            Destroy(laser.gameObject);
        }


        if (levelToLoad == (int)LevelChanger.SceneId.PlanetScene)
        {
            playerCam.farClipPlane = 500;
        }

        else if ((int)levelToLoad == (int)LevelChanger.SceneId.SpaceScene)
        {
            playerCam.farClipPlane = 1000;
        }
    }
}
