﻿using UnityEngine;

[RequireComponent(typeof(TrackerPlayer))]
[RequireComponent(typeof(TrackerEnemies))]
[RequireComponent(typeof(TrackerMisc))]
[RequireComponent(typeof(LevelChanger))]
[RequireComponent(typeof(SpaceLogic))]
public class MissionManager : MonoBehaviour
{
    private static MissionManager _instance;
    public static MissionManager Instance { get { return _instance; } }
    public bool debugMode;
    public Invincibility invincibility;

    [HideInInspector]
    public TrackerPlayer playerTracker;
    [HideInInspector]
    public TrackerEnemies enemiesTracker;
    [HideInInspector]
    public TrackerMisc miscTracker;
    [HideInInspector]
    public LevelChanger levelChanger;
    [HideInInspector]
    public SpaceLogic spaceLogic;

    [HideInInspector]
    public GameObject currTerrain;
    [HideInInspector]
    public GameObject currWeather;

    public CurrLevel currLevel;

    [HideInInspector]
    public enum Layers
    {
        PlayerInitial = 9,
        Empire = 10,
        Rebellion = 11,
        EmpireTargetArea = 12,
        RebellionTargetArea = 13,
        Capital = 14,
        Planets = 30
    }

    [HideInInspector]
    public enum Invincibility
    {
        Off = 0,
        Player = 1,
        All = 2
    }

    public enum CurrLevel
    {
        Space,
        Planet
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;

            playerTracker = this.GetComponent<TrackerPlayer>();
            enemiesTracker = this.GetComponent<TrackerEnemies>();
            miscTracker = this.GetComponent<TrackerMisc>();
            levelChanger = this.GetComponent<LevelChanger>();
            spaceLogic = this.GetComponent<SpaceLogic>();

            currLevel = CurrLevel.Space;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    
}
