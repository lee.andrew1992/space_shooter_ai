﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoader : MonoBehaviour
{
    private float LOAD_STEP_INTERVAL = 0.5f;

    public GameObject playerShip;
    public GameObject[] battles;

    public GameObject[] asteroids;
    public GameObject[] planets;

    readonly Queue <IEnumerator> loadQueue = new Queue<IEnumerator>();

    void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        SetPositionZero();
        StartLoadProcess();
        StartCoroutine(LoadGame());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SetPositionZero()
    {
        this.gameObject.transform.position = Vector3.zero;
        foreach (Transform child in this.transform)
        {
            child.position = Vector3.zero;
        }
    }

    IEnumerator SpawnPlayer()
    {
        Debug.Log("Loading Player Ship");
        GameObject player = GameObject.Instantiate(playerShip, new Vector3 (-500, 0, 0), Quaternion.identity);
        player.GetComponent<PlanetEntry>().planet = MissionManager.Instance.miscTracker.Planet;

        DontDestroyOnLoad(player);

        MissionManager.Instance.playerTracker.SetPlayerShip(player);

        yield return new WaitForSeconds(LOAD_STEP_INTERVAL);
    }

    IEnumerator SpawnBattle()
    {
        foreach (GameObject battle in battles)
        {
            GameObject b = GameObject.Instantiate(battle);
            b.GetComponent<BattleSetup>().InitiateBattle();

            DontDestroyOnLoad(b);
        }

        yield return new WaitForSeconds(LOAD_STEP_INTERVAL);
    }

    IEnumerator SpawnAsteroids()
    {
        for (int index = 0; index < asteroids.Length; index++)
        {
            Debug.Log(string.Format("Loading Asteroid {0} / {1}", index + 1, asteroids.Length));
        }

        yield return new WaitForSeconds(LOAD_STEP_INTERVAL);
    }

    IEnumerator SpawnPlanets()
    {
        for (int index = 0; index < planets.Length; index++)
        {
            GameObject planet = GameObject.Instantiate(planets[index], new Vector3(1000, 200, 500), Quaternion.AngleAxis(270, Vector3.up));

            MissionManager.Instance.miscTracker.Planet = planet;
            DontDestroyOnLoad(planet);
        }

        yield return new WaitForSeconds(LOAD_STEP_INTERVAL);
    }

    IEnumerator WaitMissionManagerAwake()
    {
        while (MissionManager.Instance == null)
        {
            Debug.Log("Wait till mission manager is loaded");
            yield return new WaitForSeconds(0.25f);
        }
    }

    private void StartLoadProcess()
    {

        loadQueue.Enqueue(WaitMissionManagerAwake());
        loadQueue.Enqueue(SpawnBattle());
        loadQueue.Enqueue(SpawnAsteroids());
        loadQueue.Enqueue(SpawnPlanets());
        loadQueue.Enqueue(SpawnPlayer());
    }

    private IEnumerator LoadGame()
    {
        while (loadQueue.Count > 0)
        {
            yield return loadQueue.Dequeue();
        }
    }
}
