﻿namespace Constant
{
    public static class Strings
    {
        public static class Controls
        {
            public static class LeftStick
            {
                public static string Horizontal = "LeftHorizontal";
                public static string Vertical = "LeftVertical";
            }

            public static class RightStick
            {
                public static string Horizontal = "RightHorizontal";
                public static string Vertical = "RightVertical";
            }

            public static class ShootStick
            {
                public static string FireGun = "FireGun";
            }
        }
    }

}